package br.com.senac.praias;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

public class DetalhesActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        Intent intent = getIntent();
        Praia praia = (Praia) intent.getSerializableExtra(ListaActivity.PRAIA);

        if (praia != null) {

            TextView textViewTitulo = (TextView) findViewById(R.id.titulo);

            TextView textViewInformacao = (TextView) findViewById(R.id.informacoes);

            textViewTitulo.setText(praia.getNome());
            textViewInformacao.setText(praia.getInformacao());

            ImageView imageViewFoto = (ImageView) findViewById(R.id.foto) ;

            imageViewFoto.setImageDrawable(getResources().getDrawable(praia.getImagem()));



            /*
            String mDrawableName = "myimageName";
int resID = res.getIdentifier(mDrawableName , "drawable", getPackageName());
imgView.setImageResource(resID);
             */

        }


    }


}
