package br.com.senac.praias;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends Activity {

    public static final String PRAIA = "praia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        // final String[] listaPraias = {"Piuma" , "Guarapari"java.lang.String , "Manguinhos"} ;

        String piuma = getResources().getString(R.string.piuma);
        String piuma_info = getResources().getString(R.string.piuma_info);

        String guarapari = getResources().getString(R.string.guarapari);
        String guarapari_info = getResources().getString(R.string.guarapari_info);

        String manguinhos = getResources().getString(R.string.manguinhos);
        String manguinhos_info = getResources().getString(R.string.manguinhos_info);


        List<Praia> lista = new ArrayList<>();
        lista.add(new Praia(1, piuma, piuma_info , R.drawable.piuma));
        lista.add(new Praia(2, guarapari, guarapari_info , R.drawable.guarapari));
        lista.add(new Praia(3, manguinhos, manguinhos_info , R.drawable.manguinhos));


        int layout = android.R.layout.simple_list_item_1;

        // ArrayAdapter adapter = new ArrayAdapter<String>(this , layout , listaPraias);

        ArrayAdapter<Praia> adapter = new ArrayAdapter<Praia>(this, layout, lista);


        ListView listView = (ListView) findViewById(R.id.Lista);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(
                    AdapterView<?> adapter,
                    View contexto,
                    int posicao,
                    long indice) {

                Praia praia = (Praia) adapter.getItemAtPosition(posicao);

                Intent intent = new Intent(ListaActivity.this, DetalhesActivity.class);
                intent.putExtra(ListaActivity.PRAIA, praia);
                startActivity(intent);



                /*
                Toast.makeText(ListaActivity.this ,
                        praia.getInformacao()   , Toast.LENGTH_SHORT).show();;
                        */
            }
        });


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            switch (item.getItemId()){
                case R.id.help:
                   Intent intent = new Intent(ListaActivity.this , HelpActivity.class);
                    startActivity(intent);
                    break;
                default:
            }

    return true;
    }

    public void novo(MenuItem item){
        Intent intent = new Intent(ListaActivity.this, novoActivity.class);
        startActivity(intent);

    }

}
